#include <stddef.h> // for size_t
#include <stdint.h> // for uint8_t
#include <stdio.h> // for printf
#include <netinet/in.h>
#include "sstream"
#include <vector>
#include <fstream>
#include <iostream>


static uint32_t read_network_byte_order_from_binary(const std::string& filePath) {
    uint32_t ret = 0;

    std::ifstream file (filePath, std::ios::binary);
    if (file.is_open())
    {
        file.read(reinterpret_cast<char *>(&ret),sizeof(ret));
    }

    file.close();

    return ntohl(ret);
}

int main(int argc, char* argv[]) {

    if (argc != 3) {
        std::cout << "Usage ./add-nbo <binary1> <binary2>" << std::endl;

        return -1;
    }

    uint32_t first = read_network_byte_order_from_binary(argv[1]);
    uint32_t second =  read_network_byte_order_from_binary(argv[2]);
    uint32_t sum = first + second;
    std::cout << std::dec << first << "(0x" << std::hex << first << ") + ";
    std::cout << std::dec << second  << "(0x" << std::hex << second << ") = ";
    std::cout << std::dec << sum << "(0x" << std::hex << sum << ")" << std::endl;
}
